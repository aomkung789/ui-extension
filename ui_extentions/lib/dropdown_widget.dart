import 'package:flutter/material.dart';

class DropDown extends StatefulWidget {
  DropDown({Key? key}) : super(key: key);

  @override
  _DropDownState createState() => _DropDownState();
}

class _DropDownState extends State<DropDown> {
  String vaccine = '-';
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('DropDown'),
      ),
      body: Column(
        children: [
          Container(
            child: Row(
              children: [
                Text('Vaccine'),
                Expanded(
                  child: Container(),
                ),
                DropdownButton(
                  items: [
                    DropdownMenuItem(child: Text('-'), value: '-'),
                    DropdownMenuItem(child: Text('Pfizer'), value: 'Pfizer'),
                    DropdownMenuItem(
                        child: Text('Jhonson & Jhonson'),
                        value: 'Jhonson & Jhonson'),
                    DropdownMenuItem(
                        child: Text('Sputnik V'), value: 'Sputnik V'),
                    DropdownMenuItem(
                        child: Text('AstraZeneca'), value: 'AstraZeneca'),
                    DropdownMenuItem(child: Text('Novavax'), value: 'Novavax'),
                    DropdownMenuItem(
                        child: Text('Sinopharm'), value: 'Sinopharm'),
                    DropdownMenuItem(child: Text('Sinovac'), value: 'Sinovac'),
                  ],
                  value: vaccine,
                  onChanged: (String? newValue) {
                    setState(() {
                      vaccine = newValue!;
                    });
                  },
                )
              ],
            ),
          ),
          Center(
            child: Text(
              vaccine,
              style: TextStyle(fontSize: 20),
            ),
          )
        ],
      ),
    );
  }
}
